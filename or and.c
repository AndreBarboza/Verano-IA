#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define epoca 2000
#define K 0.5f

//Funcion de Entrenamiento Perceptron
float EntNtand(float, float, float, float, float, float);
//Funcion para las salidas 
float InitNtand(float, float, float, float, float);

float EntNtor(float, float, float, float , float, float);
//Funcion para las salidas 
float InitNtor(float, float, float, float, float);

//Sigmoide
float sigmoide(float);
//pesos aleatorios
void pesos_initNt();

float Pesosand[5];	
float biasand=0.5f;

float Pesosor[5];	
float biasor=0.5f;
 
float EntNtand( float x0, float x1, float x2, float x3, float x4,  float target )
{
  float net = 0;
  float out = 0;
  float delta[5];  //Es la variacion de los pesos sinapticos
  float Error;
   
  net = Pesosand[0]*x0 + Pesosand[1]*x1 + Pesosand[2]*x2 + Pesosand[3]*x3 + Pesosand[4]*x4 -biasand;
  net = sigmoide( net );
   
  Error = target - net;
   
  biasand -= K*Error;  //Como el bias es siempre 1, pongo que 
                    //el bias incluye ya su peso sinaptico
   
  delta[0] = K*Error * x0;  //la variacion de los pesos sinapticos corresponde 
  delta[1] = K*Error * x1;  //al error cometido, por la entrada correspondiente
	delta[2] = K*Error * x2;
	delta[3] = K*Error * x3;
	delta[4] = K*Error * x4;
  Pesosand[0] += delta[0];  //Se ajustan los nuevos valores
  Pesosand[1] += delta[1];  //de los pesos sinapticos
   Pesosand[2] += delta[2];
   Pesosand[3] += delta[3];
   Pesosand[4] += delta[4];
  out=net;
  return out;
}

float EntNtor( float x0, float x1, float x2, float x3, float x4,  float target )
{
  float net = 0;
  float out = 0;
  float delta[5];  //Es la variacion de los pesos sinapticos
  float Error;
   
  net = Pesosor[0]*x0 + Pesosor[1]*x1 + Pesosor[2]*x2 + Pesosor[3]*x3 + Pesosor[4]*x4 -biasor;
  net = sigmoide( net );
   
  Error = target - net;
   
  biasor -= K*Error;  //Como el bias es siempre 1, pongo que 
                    //el bias incluye ya su peso sinaptico
   
  delta[0] = K*Error * x0;  //la variacion de los pesos sinapticos corresponde 
  delta[1] = K*Error * x1;  //al error cometido, por la entrada correspondiente
	delta[2] = K*Error * x2;
	delta[3] = K*Error * x3;
	delta[4] = K*Error * x4;
  Pesosor[0] += delta[0];  //Se ajustan los nuevos valores
  Pesosor[1] += delta[1];  //de los pesos sinapticos
   Pesosor[2] += delta[2];
   Pesosor[3] += delta[3];
   Pesosor[4] += delta[4];
  out=net;
  return out;
}

float InitNtor( float x0, float x1, float x2, float x3, float x4)
{
  float net = 0;
  float out = 0;
   
  net = Pesosor[0]*x0 + Pesosor[1]*x1 + Pesosor[2]*x2 + Pesosor[3]*x3 + Pesosor[4]*x4-biasor;
  net=sigmoide( net );
   
  out=net;
  return out;
}


 
float InitNtand( float x0, float x1, float x2, float x3, float x4)
{
  float net = 0;
  float out = 0;
   
  net = Pesosand[0]*x0 + Pesosand[1]*x1 + Pesosand[2]*x2 + Pesosand[3]*x3 + Pesosand[4]*x4-biasand;
  net=sigmoide( net );
   
  out=net;
  return out;
}
 
 
void pesos_initNt(void)
{
int i;
  for(  i = 0; i < 5; i++ )
  {
    Pesosand[i] = (float)rand()/RAND_MAX;
    Pesosor[i] = (float)rand()/RAND_MAX;
  }
}
 
float sigmoide( float s ){
  return (1/(1+ exp(-1*s)));
}

int main(){
  int i=0;
  float apr;
  pesos_initNt();
  while(i<epoca){
    i++;    
    printf("\nSalida Entrenamiento AND\n");
    apr=EntNtand(1,1,1,1,1,1);
    printf(     "1,1,1,1,1=%f\n",apr);
    apr=EntNtand(1,1,1,1,0,	0);
    printf(     "1,1,1,1,0=%f\n",apr);
    apr=EntNtand(1,1,1,0,0,	0);
   		 printf("1,1,1,0,0=%f\n",apr);
    apr=EntNtand(1,1,1,0,1,	0);
    	 printf("1,1,1,0,1=%f\n",apr);
	apr=EntNtand(1,1,0,1,1,	0);
   		 printf("1,1,0,1,1=%f\n",apr);
   	apr=EntNtand(1,1,0,1,0,	0);
   		 printf("1,1,0,1,0=%f\n",apr);
   	apr=EntNtand(1,1,0,0,1,	0);
   		 printf("1,1,0,0,1=%f\n",apr);
    apr=EntNtand(1,1,0,0,0,	0);
   		 printf("1,1,0,0,0=%f\n",apr);
    apr=EntNtand(1,0,1,1,1,	0);
   		 printf("1,0,1,1,1=%f\n",apr);
   	apr=EntNtand(1,0,1,1,0,	0);
   		 printf("1,0,1,1,0=%f\n",apr);
   	apr=EntNtand(1,0,1,0,1,	0);
   		 printf("1,0,1,0,1=%f\n",apr);
   	apr=EntNtand(1,0,1,0,0,	0);
   		 printf("1,0,1,0,0=%f\n",apr);
   	apr=EntNtand(1,0,0,1,1,	0);
   		 printf("1,0,0,1,1=%f\n",apr);
   	apr=EntNtand(1,0,0,1,0,	0);
   		 printf("1,0,0,1,0=%f\n",apr);
   	apr=EntNtand(1,0,0,0,1,	0);
   		 printf("1,0,0,0,1=%f\n",apr);
    apr=EntNtand(1,0,0,0,0,	0);
   		 printf("1,0,0,0,0=%f\n",apr);
    apr=EntNtand(0,1,1,1,1,	0);
   		 printf("0,1,1,1,1=%f\n",apr);
   	apr=EntNtand(0,1,1,1,0,	0);
   		 printf("0,1,1,1,0=%f\n",apr);
   	apr=EntNtand(0,1,1,0,1,	0);
   		 printf("0,1,1,0,1=%f\n",apr);
   	apr=EntNtand(0,1,1,0,0,	0);
   		 printf("0,1,1,0,0=%f\n",apr);
   	apr=EntNtand(0,1,0,1,1,	0);
   		 printf("0,1,0,1,1=%f\n",apr);
   	apr=EntNtand(0,1,0,1,0,	0);
   		 printf("0,1,0,1,0=%f\n",apr);
   	apr=EntNtand(0,1,0,0,1,	0);
   		 printf("0,1,0,0,1=%f\n",apr);
   	apr=EntNtand(0,1,0,0,0,	0);
   		 printf("0,1,0,0,0=%f\n",apr);
   	apr=EntNtand(0,0,1,1,1,	0);
   		 printf("0,0,1,1,1=%f\n",apr);
	apr=EntNtand(0,0,1,1,0,	0);
   		 printf("0,0,1,1,0=%f\n",apr);
	apr=EntNtand(0,0,1,0,1,	0);
   		 printf("0,0,1,0,1=%f\n",apr);
	apr=EntNtand(0,0,1,0,0,	0);
   		 printf("0,0,1,0,0=%f\n",apr);
	apr=EntNtand(0,0,0,1,1,	0);
   		 printf("0,0,0,1,1=%f\n",apr);
	apr=EntNtand(0,0,0,1,0,	0);
   		 printf("0,0,0,1,0=%f\n",apr);
   	apr=EntNtand(0,0,0,0,1,	0);
   		 printf("0,0,0,0,1=%f\n",apr);
   	apr=EntNtand(0,0,0,0,0,	0);
   		 printf("0,0,0,0,0=%f\n",apr);
    printf("\n"); 
    printf("Pesos de cada epoca\n");
    printf("Peso 1 = %f\n", Pesosand[0]);
    printf("Peso 2 = %f\n", Pesosand[1]);
    printf("Peso 3 = %f\n", Pesosand[2]);
    printf("Peso 4 = %f\n", Pesosand[3]);
    printf("Peso 5 = %f\n", Pesosand[4]);
    printf("Bias AND");
    printf("Bias = %f",biasand);
    
printf("\nSalida Entrenamiento OR\n");
    apr=EntNtor(1,1,1,1,1,1);
    printf(     "1,1,1,1,1=%f\n",apr);
    apr=EntNtor(1,1,1,1,0,	1);
    printf(     "1,1,1,1,0=%f\n",apr);
    apr=EntNtor(1,1,1,0,0,	1);
   		 printf("1,1,1,0,0=%f\n",apr);
    apr=EntNtor(1,1,1,0,1,	1);
    	 printf("1,1,1,0,1=%f\n",apr);
	apr=EntNtor(1,1,0,1,1,	1);
   		 printf("1,1,0,1,1=%f\n",apr);
   	apr=EntNtor(1,1,0,1,0,	1);
   		 printf("1,1,0,1,0=%f\n",apr);
   	apr=EntNtor(1,1,0,0,1,	1);
   		 printf("1,1,0,0,1=%f\n",apr);
    apr=EntNtor(1,1,0,0,0,	1);
   		 printf("1,1,0,0,0=%f\n",apr);
    apr=EntNtor(1,0,1,1,1,	1);
   		 printf("1,0,1,1,1=%f\n",apr);
   	apr=EntNtor(1,0,1,1,0,	1);
   		 printf("1,0,1,1,0=%f\n",apr);
   	apr=EntNtor(1,0,1,0,1,	1);
   		 printf("1,0,1,0,1=%f\n",apr);
   	apr=EntNtor(1,0,1,0,0,	1);
   		 printf("1,0,1,0,0=%f\n",apr);
   	apr=EntNtor(1,0,0,1,1,	1);
   		 printf("1,0,0,1,1=%f\n",apr);
   	apr=EntNtor(1,0,0,1,0,	1);
   		 printf("1,0,0,1,0=%f\n",apr);
   	apr=EntNtor(1,0,0,0,1,	1);
   		 printf("1,0,0,0,1=%f\n",apr);
    apr=EntNtor(1,0,0,0,0,	1);
   		 printf("1,0,0,0,0=%f\n",apr);
    apr=EntNtor(0,1,1,1,1,1);
   		 printf("0,1,1,1,1=%f\n",apr);
   	apr=EntNtor(0,1,1,1,0,1);
   		 printf("0,1,1,1,0=%f\n",apr);
   	apr=EntNtor(0,1,1,0,1,1);
   		 printf("0,1,1,0,1=%f\n",apr);
   	apr=EntNtor(0,1,1,0,0,	1);
   		 printf("0,1,1,0,0=%f\n",apr);
   	apr=EntNtor(0,1,0,1,1,	1);
   		 printf("0,1,0,1,1=%f\n",apr);
   	apr=EntNtor(0,1,0,1,0,	1);
   		 printf("0,1,0,1,0=%f\n",apr);
   	apr=EntNtor(0,1,0,0,1,1);
   		 printf("0,1,0,0,1=%f\n",apr);
   	apr=EntNtor(0,1,0,0,0,	1);
   		 printf("0,1,0,0,0=%f\n",apr);
   	apr=EntNtor(0,0,1,1,1,	1);
   		 printf("0,0,1,1,1=%f\n",apr);
	apr=EntNtor(0,0,1,1,0,	1);
   		 printf("0,0,1,1,0=%f\n",apr);
	apr=EntNtor(0,0,1,0,1,	1);
   		 printf("0,0,1,0,1=%f\n",apr);
	apr=EntNtor(0,0,1,0,0,	1);
   		 printf("0,0,1,0,0=%f\n",apr);
	apr=EntNtor(0,0,0,1,1,	1);
   		 printf("0,0,0,1,1=%f\n",apr);
	apr=EntNtor(0,0,0,1,0,	1);
   		 printf("0,0,0,1,0=%f\n",apr);
   	apr=EntNtor(0,0,0,0,1,	1);
   		 printf("0,0,0,0,1=%f\n",apr);
   	apr=EntNtor(0,0,0,0,0,	0);
   		 printf("0,0,0,0,0=%f\n",apr);
    printf("\n");
    printf("Pesos de cada epoca\n");
    printf("Peso 1 = %f\n", Pesosor[0]);
    printf("Peso 2 = %f\n", Pesosor[1]);
    printf("Peso 3 = %f\n", Pesosor[2]);
    printf("Peso 4 = %f\n", Pesosor[3]);
    printf("Peso 5 = %f\n", Pesosor[4]);
    printf("Bias OR");
    printf("Bias = %f",biasor);

}
printf("Resultados AND\n");
    apr=InitNtand(1,1,1,1,1);
    printf("1,1,1,1,1=%f\n",apr);
    apr=InitNtand(1,0,0,1,0);
    printf("1,0,0,1,0=%f\n",apr);
    apr=InitNtand(0,1,0,1,1);
    printf("0,1,0,1,1=%f\n",apr);
    apr=InitNtand(0,0,0,0,0);
    printf("0,0,0,0,0=%f\n",apr);


printf("Resultados OR\n");
    apr=InitNtor(1,1,1,1,1);
    printf("1,1,1,1,1=%f\n",apr);
    apr=InitNtor(1,0,0,1,0);
    printf("1,0,0,1,0=%f\n",apr);
    apr=InitNtor(0,1,0,1,1);
    printf("0,1,0,1,1=%f\n",apr);
    apr=InitNtor(0,0,0,0,0);
    printf("0,0,0,0,0=%f\n",apr);
  return 0;



}
