package funciontsela;
import java.util.Scanner;
/**
 *
 * @author DiegoAndre
 */
public class Funciontsela {
    static String vector[][]=new String[8][8];
    public static void main(String[] args) {
        relleno();
        Scanner leer=new Scanner(System.in);
        System.out.println("donde empiezo?");
        System.out.println("coordenadas en x: ");
        int x=leer.nextInt();
        System.out.println("coordenadas en y: ");
        int y=leer.nextInt();
        empezar(x-1,y-1, 1);
    }
    static void relleno(){
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                vector[i][j]="*";
            }
        }
    }
    static void empezar(int x, int y, int op){
        int termine=check();
        if (termine==1) {
            imprimir();
        }
        else{
         
        switch(op){
            case 1:
                if (x<7 && y>0) {
                    dedown(x,y);
                    empezar(x++,y--,2);
                }
                else{
                    
                }
                
                break;
            case 2:
                izdown(x,y);
                break;
            case 3:
                izup(x,y);
                break;
            case 4:
                dedown(x,y);
            
        }
            
            
    }}
    
    static void izup(int x, int y){
            vector[x][y]="O";
            vector[x-1][y]="O";
            vector[x][y-1]="O";
    }
    static void izdown(int x, int y){
            vector[x][y]="O";
            vector[x-1][y]="O";
            vector[x][y+1]="O";
    }
    static void deup(int x, int y){
            vector[x][y]="O";
            vector[x+1][y]="O";
            vector[x][y-1]="O";
    }
    static void dedown(int x, int y){
            vector[x][y]="O";
            vector[x-1][y]="O";
            vector[x][y+1]="O";
    }
    static void imprimir(){
        System.out.println("");
        System.out.println("");
        for (int i = 0; i < 8; i++) {
            System.out.println("");
            for (int j = 0; j < 8; j++) {
                System.out.print(vector[i][j]);
                System.out.print(" ");
            }
        }
    }
    static int check(){
        int op=0;
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                if (vector[i][j]=="*") {
                    op++;
                }
            }
        }
        return op;
    }
}
