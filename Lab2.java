package lab2;

/**
 *
 * @author Barboza
 */
public class Lab2 {
    
    static int numeros[][]={
                     // 0   1   2   3   4   5   6   7   8   9  10  11  12  13  14   
                       {0,  0,  1,  1,  1,  1,  1,  1,  1,  0,  0,  1,  0, 80,100},//0
                       {1,  0,  1,  1,  0,  0,  1,  0,  1,  0,  1,  0, 30,  1,   0},//1
                       {1,  0,  0,  0,  0,  0,  5,  0,  8,  0, 10,  0,  0, 20,  0},//2
                       {1, 15,  1,  1,  1, 15,  1,  0,  1, 15,  1, 15,  1, 10,  0},//3
                       {10, 0,  0,  0, 10,  0, 15,  0,  1,  0,  1,  0,  0,  1,  0},//4
                       {10, 1,  1,  0, 10,  0,  1,  1,  1,  0,  1,  0,  1,  1,  0},//5
                       {10, 0,  1,  0, 10,  0,  0,  0,  0,  0,  1,  0,  0,  1,  0},//6
                       {10, 0,  1,  0, 10,  1,  1,  1,  1,  1,  1,  1,  1,  1,  0}};//7        
    static boolean termine=false;
    static int x=0, y=0;
    static String laberinto[][]={
                     // 0   1   2   3   4   5   6   7   8   9  10  11  12  13  14   
                      {"M","#"," "," "," "," "," "," "," ","#","#"," ","#"," ","S"},//0
                      {" ","#"," "," ","#","#"," ","#"," ",""," ","#"," "," ","#"},//1
                      {" ","#","#","#","#","#"," ","#"," ","#"," ","#","#"," ","#"},//2
                      {" "," "," "," "," "," "," ","#"," "," "," "," "," "," ","#"},//3
                      {" ","#","#","#"," ","#"," ","#"," ","#"," ","#","#"," ","#"},//4
                      {" "," "," ","#"," ","#"," "," "," ","#"," ","#"," "," ","#"},//5
                      {" ","#"," ","#"," ","#","#","#","#","#"," ","#","#"," ","#"},//6
                      {" ","#"," ","#"," "," "," "," "," "," "," "," "," "," ","#"}};//7
    public static void main(String[] args) {
        avanzar();
    }
    static int oeste(){
        if (x>0) {//avanza oeste
                    if (laberinto[y][x-1]=="S") {
                        termine=true;
                    }
                    if (laberinto[y][x-1]!="#") {
                        laberinto[y][x]="O";
                        laberinto[y][x-1]="M";
                        numeros[y][x]=0;
                        x--;
                    }}
        return 4;
    }
    static int este(){
        if(x<15){//este
                    if (laberinto[y][x+1]=="S") {
                        termine=true;
                    }
                    if (laberinto[y][x+1]!="#") {
                        laberinto[y][x]="O";
                        laberinto[y][x+1]="M";
                        numeros[y][x]=0;
                        x++;
                    }}
        return 2;
    }
    static int sur(){
        if(y<7){//avanza sur
                    if (laberinto[y+1][x]=="S") {
                        termine=true;
                    }
                    if (laberinto[y+1][x]!="#") {
                        laberinto[y][x]="O";
                        laberinto[y+1][x]="M";
                        numeros[y][x]=0;
                        y++;
                    }
        }
        return 1;
    }
    static int norte(){
        if (y>0) {//norte
                    if (laberinto[y-1][x]=="S") {
                        termine=true;
                    }
                    if (laberinto[y-1][x]!="#") {
                        laberinto[y][x]="O";
                        laberinto[y-1][x]="M";
                        numeros[y][x]=0;
                        y--;
                        
                    }}   
        return 3;
    }
    static void avanzar(){
        int w=0, s=0, a=0, d=0;
        if (x>0) {
            a=numeros[y][x-1];
        }
        if (x<15) {
            d=numeros[y][x+1];
        }
        if (y>0) {
            w=numeros[y-1][x];
        }
        if (y<7) {
            s=numeros[y+1][x];
        }
        if (w>s && w>d && w>a) {
            norte();
        }
        if (s>w && s>d && s>a) {
            sur();
        }
        if (d>w && d>a && d>s) {
            este();
        }
        if (a>s && a>w && a>d) {
            oeste();
        }
        imprimir();
        if (!termine) {
            avanzar();
        }else{
            imprimir();
        }
               
        }

    static void  imprimir(){
        System.out.println("");
        System.out.println("");
        for (int i = 0; i < 8; i++) {
            System.out.println("");
            for (int j = 0; j < 15; j++) {
                System.out.print(laberinto[i][j]+"  ");
            }
        }
    }    
}