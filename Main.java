/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg5x5;
import java.util.Scanner;

/**
 *
 * @author DiegoAndre
 */
public class Main {
    static int mapa[][]={{1,1,1,1,1},
                         {1,1,1,1,1},
                         {1,1,1,1,1},
                         {1,1,1,1,1},
                         {1,1,1,1,1}};
    static int flag=0;
    public static void main(String[] args) {
        Scanner leer=new Scanner(System.in);
        System.out.println("Donde empiezo?");
        System.out.println("X: ");
        int x=leer.nextInt();
        System.out.println("Y: ");
        int y=leer.nextInt();
        recorre(x,y);
        imprimir();
        
    }
    static void imprimir(){
        for (int i = 0; i < 5; i++) {
            System.out.println("");
            for (int j = 0; j < 5; j++) {
                System.out.print(mapa[i][j]);
            }
        }
        System.out.println("");
        System.out.println("");
    }
    static void recorre(int x, int y){
        imprimir();
        flag=0;
        mapa[x][y]=0;
           flag+=a(x,y);//arriba
           flag+=b(x,y);//arriba derecha
           flag+=c(x,y);//derecha
           flag+=d(x,y);//abajo derecha
           flag+=e(x,y);//abajo
           flag+=f(x,y);//abajo izquierda
           flag+=g(x,y);//izquierda
           flag+=h(x,y);//izquierda arriba
           System.out.println(flag);
           
           
           
           
    }
    static int a(int x,int y){
        int o=0;
        if (x>0) {
            if (mapa[x-1][y]==1) {
                recorre(x-1,y);
                o=1;
            }
        }
            return o;
    }
    static int b(int x,int y){
        int o=0;
        if (x>0 && y<4) {
            if (mapa[x-1][y+1]==1) {
                recorre(x-1,y+1);
                o++;
            }
        }
        return o;
    }
    static int c(int x,int y){
        int o=0;
        if (y<4) {
            if (mapa[x][y+1]==1) {
                recorre(x,y+1);
                o++;
            }
        }
        return o;
    }
    static int d(int x,int y){
        int o=0;
        if (x<4 && y<4) {
            if (mapa[x+1][y+1]==1) {
                recorre(x+1,y+1);
                o++;
            }
        }
        return o;
    }
    static int e(int x,int y){
        int o=0;
        if (x<4) {
            if (mapa[x+1][y]==1) {
                recorre(x+1,y);
                o++;
            }
        }
        return o;
    }
    static int f(int x,int y){
        int o=0;
        if (x<4 && y>0) {
            if (mapa[x+1][y-1]==1) {
                recorre(x+1,y-1);
                o++;
            }
        }
        return o;
    }
    static int g(int x,int y){
        int o=0;
        if (y>0) {
            if (mapa[x][y-1]==1) {
                recorre(x,y-1);
                o++;
            }
        }
        return o;
    }
    static int h(int x,int y){
        int o=0;
        if (x>0 && y>0) {
            if (mapa[x-1][y-1]==1) {
                recorre(x-1,y-1);
                o++;
            }
        }
        return o;
    }
        
}
