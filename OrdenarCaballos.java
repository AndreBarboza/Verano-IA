package ordenarcaballos;
public class OrdenarCaballos {
    static String tablero[][]=new String[3][3];
    public static void main(String[] args) {                
        tablero[0][0]="1";
        tablero[0][2]="2";
        tablero[2][0]="3";
        tablero[2][2]="4";
        tablero[0][1]="*";
        tablero[1][0]="*";
        tablero[1][1]="*";
        tablero[1][2]="*";
        tablero[2][1]="*";
        System.out.println("Inicio:");
        imprimir();        
        movimientos();        
                                        
    }

    public static void movimientos() {
        
        for (int i = 1; i <=16; i++) {
            switch(i){
                case 1:
                    tablero[1][2]="1";
                    tablero[0][0]="*";
                    System.out.println("\nTurno "+i);
                    imprimir();
                    break;
                case 2:
                    tablero[0][1]="3";
                    tablero[2][0]="*";
                    System.out.println("\nTurno "+i);
                    imprimir();
                    break;
                case 3:
                    tablero[1][0]="4";
                    tablero[2][2]="*";
                    System.out.println("\nTurno "+i);
                    imprimir();
                    break;
                case 4:
                    tablero[2][1]="2";
                    tablero[0][2]="*";
                    System.out.println("\nTurno "+i);
                    imprimir();
                    break;
                    
                case 5:
                    tablero[2][0]="1";
                    tablero[1][2]="*";
                    System.out.println("\nTurno "+i);
                    imprimir();
                    break;
                case 6:
                    tablero[2][2]="3";
                    tablero[0][1]="*";
                    System.out.println("\nTurno "+i);
                    imprimir();
                    break;
                case 7:
                    tablero[0][2]="4";
                    tablero[1][0]="*";
                    System.out.println("\nTurno "+i);
                    imprimir();
                    break;
                case 8:
                    tablero[0][0]="2";
                    tablero[2][1]="*";
                    System.out.println("\nTurno "+i);
                    imprimir();
                    break;
                    
                case 9:
                    tablero[0][1]="1";
                    tablero[2][0]="";
                    System.out.println("\nTurno "+i);
                    imprimir();
                    break;
                case 10:
                    tablero[1][0]="3";
                    tablero[2][2]="*";
                    System.out.println("\nTurno "+i);
                    imprimir();
                    break;
                case 11:
                    tablero[2][1]="4";
                    tablero[0][2]="*";
                    System.out.println("\nTurno "+i);
                    imprimir();
                    break;
                case 12:
                    tablero[1][2]="2";
                    tablero[0][0]="*";
                    System.out.println("\nTurno "+i);
                    imprimir();
                    break;
                    
                case 13:
                    tablero[2][2]="1";
                    tablero[0][1]="*";
                    System.out.println("\nTurno "+i);
                    imprimir();
                    break;
                case 14:
                    tablero[0][2]="3";
                    tablero[1][0]="*";
                    System.out.println("\nTurno "+i);
                    imprimir();
                    break;
                case 15:
                    tablero[0][0]="4";
                    tablero[2][1]="*";
                    System.out.println("\nTurno "+i);
                    imprimir();
                    break;
                case 16:
                    tablero[2][0]="2";
                    tablero[1][2]="*";
                    System.out.println("\nTurno "+i);
                    imprimir();
                    break;}}
            }
    
    
    public static void imprimir(){
        for (int i = 0; i < tablero.length; i++) {
            for (int j = 0; j < tablero[i].length; j++) {
                System.out.print(tablero[i][j]+"    ");
            }
            System.out.println();
        }}}