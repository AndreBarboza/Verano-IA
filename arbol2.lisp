(defparameter *arbol* '(
                        ("madre" 
                            ("hijos"
                                ("Jimmy" (jimi es un hijo de 3))
                                ("Abril" (abril es un hijo de 3))
                                ("Mayo" (mayo es un hijo de 3))
                            )
                            
                        )
                        
                        ("borracho" 
                                ("ayer"
                                    ("fuese"
                                        ("mañana"
                                            ("viernes" (el dia es miercoles))
                                        )
                                    )
                                )
                        )
                        
                        ("vaquero"
                            ("Viernes"
                                ("Viernes" (El nombre de su caballo es Viernes))
                            )
                        )
                        
                        ("tres"
                            ("parecen"
                                ("existirian"
                                    ("convierte"
                                        ("soberanos"
                                        )
                                    )
                                )
                            
                            )
                        
                        )
                        
                        ))
                        
(defun frase(*a*)
    (defparameter x (list "la" "madre" "de" "Jimmy" "tiene" "varios" "hijos" "el" "primero" "se" "llama" "Abril" "el" "segundo" "se" "llama" "Mayo" "cual" "es" "el" "nombre" "del" "tercero"))
    (defparameter y (list "un" "borracho" "dijo" "si" "ayer" "fuese" "mañana" "hoy" "seria" "viernes" "que" "dia" "es" "hoy?"))
    (defparameter v (list "tres" "hermanos" "viven" "en" "una" "casa" "son" "de" "veras" "diferentes" "si" "quieres" "distingurlos" "los" "3" "se" "parecen" "el" "primero" "no" "esta" "ha" "de" "venir" "el" "segundo" "no" "esta" "ya" "se" "fue" "el" "tercero" "menor" "que" "todos" "sin" "no" "existirian" "los" "otros" "aun" "asi" "el" "tercero" "solo" "existe" "porque" "en" "el" "segundo" "se" "convierte" "el" "primero" "si" "quieres" "mirarlo" "no" "ves" "mas" "que" "otro" "de" "sus" "hermanos" "los" "3" "son" "uno"))
    (r x *a* 0 0)
    (r y *a* 0 0)

)


(defun r(lista arbol n m)
	(if (equal m 0)
	(progn
	(set 'cero (nth 0 arbol))
	(set 'uno (nth 1 arbol))
	(set 'dos (nth 2 arbol))
	(set 'dos (nth 3 arbol)))
	(set 'm 1)
	)
	(print (car cero))
	(if (equal n 0)
	(loop for x in lista
     do 	(if (string= (car cero) x)
      					(progn 
      						(print cero)
      						(r lista (cdr (assoc x cero) n m)
      						))
    ))

	(if (equal n 1)
    (loop for x in lista
     do 	(if (string= (car uno) x)
      					(progn 
      						(print uno)
      						(r lista (cdr (assoc x uno) n m)
      						))
    ))

    (if (equal n 2)
    (loop for x in lista
     do 	(if (string= (car dos) x)
      					(progn 
      						(print dos)
      						(r lista (cdr (assoc x dos) n m)
      						))
    ))
			
)