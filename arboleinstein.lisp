(defparameter *arbol* '((noruego 
							(amarilla(la casa es amarilla)) 
							(gato(su mascota es el gato))
							(agua(bebe agua))
							(dunhill(fuma dunhill)))
						(aleman
							(verde(vive en la casa verde))
							(pez(tiene un pez))
							(cafe(bebe cafe))
							(prince(fuma prince)))
						(danes
							(azul(su casa es azul))
							(caballo(su mascota es un caballo))
							(te(bebe te))
							(bends(fuma bends)))
						(sueco
							(blanca(su casa es blanca))
							(perro(su mascota es perro))
							(cerveza(bebe cerveza))
							(blue(fuma blue)))
						(britanico
							(roja(su casa es roja))
							(pajaro(su mascota es un pajaro))
							(pallmall(fuma pallmall))
							(leche(leche)))))

(defun recorre( *arbol)
	(format t "dame el color de su casa, que tipo de mascota tiene, que cigarrillos fuma o que bebe y te dire quien es: ")
	(set 'caracteristica (read))
	(set 'noru (cdr(assoc 'noruego *arbol)))
	(set 'ale  (cdr(assoc 'aleman *arbol)))
	(set 'dan  (cdr(assoc 'danes *arbol)))
	(set 'sue  (cdr(assoc 'sueco *arbol)))
	(set 'bri  (cdr(assoc 'britanico *arbol)))
	(set 'n (cdr(assoc caracteristica noru)))
	(set 'a (cdr(assoc caracteristica ale)))
	(set 'd (cdr(assoc caracteristica dan)))
	(set 's (cdr(assoc caracteristica sue)))
	(set 'b (cdr(assoc caracteristica bri)))
	(if (equal n nil)
		(if (equal a nil)
			(if (equal d nil)
				(if (equal s nil)
					(if (equal b nil)
						(print "no encontre a nadie con esa caracteristica")
						(print "britanico"))
				(print "sueco")
				)
			(print "danes")
			)
		(print "noruego"))))
