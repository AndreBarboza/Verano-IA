
package puzzle8;

/**
 *
 * @author DiegoAndre
 */
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Puzzle8 {
    public static void main(String args[]) {
        Puzzle eightPuzzle = new Puzzle();
    }
    static class Puzzle {
        int matrix[][]={{4, 3, 0},{2, 5, 7},{6, 8, 1}};
        int simpleArrayForm[] = new int[8];
        int simpleArrayFormCounter = 0;
        int rowBlank, colBlank;

        Random r;
        List<Integer> numbers;

        Puzzle() {
            r = new Random();
            numbers = new ArrayList<>();
            //fillMatrix();
            imprimir();
            for (int i = 0; i < 3; i++) {
                for (int j = 0; j < 3; j++) {
                    if (matrix[i][j]==0) {
                        rowBlank=i;
                        colBlank=j;
                    }
                }
            }
            if (checar()){
                resolver();
                imprimir();
            }
            else{
                System.out.println("No se puede resolver el problema");
            }

        }

        void resolver() {
            while (true) {
                if (matrix[0][0] == 1 && matrix[0][1] == 2 && matrix[0][2] == 3 && matrix[1][0] == 4 && matrix[1][1] == 5 && matrix[1][2] == 6 && matrix[2][0] == 7 && matrix[2][1] == 8) {
                    break;
                }
                imprimir();
                mover(r.nextInt(4));
            }
        }

        void mover(int direction) {
            switch (direction) {
                case 0:
                    if (rowBlank - 1 >= 0) {
                        int numberToExchange = matrix[rowBlank - 1][colBlank];
                        matrix[rowBlank][colBlank] = numberToExchange;
                        matrix[rowBlank - 1][colBlank] = 0;
                        rowBlank--;
                    }
                    break;
                case 1:
                    if (rowBlank + 1 < 3) {
                        int numberToExchange = matrix[rowBlank + 1][colBlank];
                        matrix[rowBlank][colBlank] = numberToExchange;
                        matrix[rowBlank + 1][colBlank] = 0;
                        rowBlank++;
                    }
                    break;
                case 2:
                    if (colBlank - 1 >= 0) {
                        int numberToExchange = matrix[rowBlank][colBlank - 1];
                        matrix[rowBlank][colBlank] = numberToExchange;
                        matrix[rowBlank][colBlank - 1] = 0;
                        colBlank--;
                    }
                    break;
                case 3:
                    if (colBlank + 1 < 3) {
                        int numberToExchange = matrix[rowBlank][colBlank + 1];
                        matrix[rowBlank][colBlank] = numberToExchange;
                        matrix[rowBlank][colBlank + 1] = 0;
                        colBlank++;
                    }
                    break;
            }
        }

        @SuppressWarnings("empty-statement")
        void fillMatrix() {
            for (int i = 0; i < 3; i++) {
                for (int j = 0; j < 3; j++) {
                    while (true) {
                        int number = r.nextInt(9);
                        if (!numbers.contains(number)) {
                            matrix[i][j] = number;
                            numbers.add(number);
                            if (number == 0) {
                                rowBlank = i;
                                colBlank = j;
                            }
                            else{
                                simpleArrayForm[simpleArrayFormCounter] = number;
                                simpleArrayFormCounter++;
                            }
                            break;
                        }
                    }
                }
            }
            
            
        }

        boolean checar(){
            int in = 0;
            for(int i = 0; i<8 ; i++){
                for(int j = i+1; j<8; j++){
                    if(simpleArrayForm[j]>simpleArrayForm[i]){
                        in++;
                    }
                }
            }
            return (in % 2 == 0)? true : false;
        }

        void imprimir() {
            for (int i = 0; i < 3; i++) {
                for (int j = 0; j < 3; j++) {
                    System.out.print(matrix[i][j] + " ");
                }
                System.out.println("");
            }
            System.out.println("");
        }
    }
}

