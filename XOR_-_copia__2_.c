#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define epoca 1000
#define K 0.5f

//Funcion de Entrenamiento Perceptron
float EntNt(float, float, float);
//Funcion para las salidas 
float InitNt(float, float);
//Sigmoide
float sigmoideand(float);
float sigmoideor(float);
float sigmoidexor(float);
//pesos aleatorios
void pesos_initNtand();
void pesos_initNtor();
void pesos_initNtxor();

float Pesosand[2];
float Pesosor[2];
float Pesosxor[2];
float biasand=0.5f;
float biasor=0.5f;
float biasxor=0.5f;

 
float EntNtand( float x0, float x1, float target )
{
  float net = 0;
  float out = 0;
  float delta[2];  //Es la variacion de los pesos sinapticos
  float Error;
   
  net = Pesosand[0]*x0 + Pesosand[1]*x1-biasand;
  net = sigmoideand( net );
   
  Error = target - net;
   
  biasand -= K*Error;  //Como el bias es siempre 1, pongo que 
                    //el bias incluye ya su peso sinaptico
   
  delta[0] = K*Error * x0;  //la variacion de los pesos sinapticos corresponde 
  delta[1] = K*Error * x1;  //al error cometido, por la entrada correspondiente
   
  Pesosand[0] += delta[0];  //Se ajustan los nuevos valores
  Pesosand[1] += delta[1];  //de los pesos sinapticos
   
  out=net;
  return out;
}
float EntNtor( float x0, float x1, float target )
{
  float net = 0;
  float out = 0;
  float delta[2];  //Es la variacion de los pesos sinapticos
  float Error;
   
  net = Pesosor[0]*x0 + Pesosor[1]*x1-biasor;
  net = sigmoideor( net );
   
  Error = target - net;
   
  biasor -= K*Error;  //Como el bias es siempre 1, pongo que 
                    //el bias incluye ya su peso sinaptico
   
  delta[0] = K*Error * x0;  //la variacion de los pesos sinapticos corresponde 
  delta[1] = K*Error * x1;  //al error cometido, por la entrada correspondiente
   
  Pesosor[0] += delta[0];  //Se ajustan los nuevos valores
  Pesosor[1] += delta[1];  //de los pesos sinapticos
   
  out=net;
  return out;
}
float EntNtxor( float x0, float x1, float target )
{
  float net = 0;
  float out = 0;
  float delta[2];  //Es la variacion de los pesos sinapticos
  float Error;
   
  net = Pesosxor[0]*x0 + Pesosxor[1]*x1-biasxor;
  net = sigmoidexor( net );
   
  Error = target - net;
   
  biasxor -= K*Error;  //Como el bias es siempre 1, pongo que 
                    //el bias incluye ya su peso sinaptico
   
  delta[0] = K*Error * x0;  //la variacion de los pesos sinapticos corresponde 
  delta[1] = K*Error * x1;  //al error cometido, por la entrada correspondiente
   
  Pesosxor[0] += delta[0];  //Se ajustan los nuevos valores
  Pesosxor[1] += delta[1];  //de los pesos sinapticos
   
  out=net;
  return out;
}


 
float InitNtand( float x0, float x1 )
{
  float net = 0;
  float out = 0;
   
  net = Pesosand[0]*x0 + Pesosand[1]*x1-biasand;
  net=sigmoideand( net );
   
  out=net;
  return out;
}
float InitNtor( float x0, float x1 )
{
  float net = 0;
  float out = 0;
   
  net = Pesosand[0]*x0 + Pesosand[1]*x1-biasand;
  net=sigmoideand( net );
   
  out=net;
  return out;
}
float InitNtxor( float x0, float x1 )
{
  float net = 0;
  float out = 0;
   
  net = Pesosxor[0]*x0 + Pesosxor[1]*x1-biasxor;
  net=sigmoidexor( net );
   
  out=net;
  return out;
}
 
 
void pesos_initNtand(void)
{
int i;
  for(  i = 0; i < 2; i++ )
  {
    Pesosand[i] = (float)rand()/RAND_MAX;
  }
}
void pesos_initNtor(void)
{
int i;
  for(  i = 0; i < 2; i++ )
  {
    Pesosand[i] = (float)rand()/RAND_MAX;
  }
}

void pesos_initNtxor(void)
{
int i;
  for(  i = 0; i < 2; i++ )
  {
    Pesosxor[i] = (float)rand()/RAND_MAX;
  }
}
 
float sigmoideand( float s ){
  return (1/(1+ exp(-1*s)));
}

float sigmoideor( float s ){
  return (1/(1+ exp(-1*s)));
}

float sigmoidexor( float s ){
  return (1/(1+ exp(-1*s)));
}

int main(){
  int i=0;
  float apr;
  pesos_initNtor();
  while(i<epoca){
    i++;    
    printf("Salida Entrenamiento or:\n");
    apr=EntNtor(1,1,1);
    printf("1,1=%f\n",apr);
    apr=EntNtor(1,0,1);
    printf("1,0=%f\n",apr);
    apr=EntNtor(0,1,1);
    printf("0,1=%f\n",apr);
    apr=EntNtor(0,0,0);
    printf("0,0=%f\n",apr);
    printf("\n"); 
	printf("Salida Entrenamiento and:\n");
    apr=EntNtand(1,1,1);
    printf("1,1=%f\n",apr);
    apr=EntNtand(1,0,0);
    printf("1,0=%f\n",apr);
    apr=EntNtand(0,1,0);
    printf("0,1=%f\n",apr);
    apr=EntNtand(0,0,0);
    printf("0,0=%f\n",apr);
    printf("\n"); 
    printf("Salida Entrenamiento xor:\n");
    apr=EntNtxor(1,1,0);
    printf("1,1=%f\n",apr);
    apr=EntNtxor(1,0,1);
    printf("1,0=%f\n",apr);
    apr=EntNtxor(1,0,1);
    printf("0,1=%f\n",apr);
    apr=EntNtxor(0,0,0);
    printf("0,0=%f\n",apr);
    printf("\n"); 
}
getch();
  return 0;



}
